<?php

/**
 * Created by IntelliJ IDEA.
 * User: Werner
 * Date: 09.06.2016
 * Time: 15:17
 */
class OrderPaidTestTask extends BuildTask
{

	protected $enabled = false;

	/**
	 * Implement this method in the task subclass to
	 * execute via the TaskRunner
	 * @param $request
	 */
	public function run($request)
	{
		$orderID = 7653;

		$order = Order::get()->byID($orderID);

		$payment = Payment::get()->filter(['OrderID' => $orderID])->first();

		$payment->extend('onAuthorized', $orderID);
	}
}
