<?php

use TheIconic\Tracking\GoogleAnalytics\Analytics;

/**
 * Created by IntelliJ IDEA.
 *
 * User: Werner
 * Date: 09.06.2016
 * Time: 14:33
 *
 * @property Payment|ExtendedGoogleAnalyticsPaymentExtension $owner
 */
class ExtendedGoogleAnalyticsPaymentExtension extends DataExtension
{

	/**
	 * when payment gets authorized, track to Google Analytics using PHP gateway
	 * @param $response
	 */
	public function onAuthorized($response)
	{
		$order = $this->owner->Order();

		if ($order->AnalyticsSubmitted) {
			return;
		}

		$analytics = $this->analyticsFactory($order);

		$analytics->setTransactionId($order->ID);
		$analytics->setCheckoutStep(2)
			->setEventCategory('Checkout')
			->setEventAction('Payment Received')
			->setEventLabel('Payment Received');
			//->sendEvent();


		$analytics = $this->analyticsFactory($order);
		$analytics->setTransactionId($order->ID)
			->setRevenue($order->Total)
			->setTax($order->GATaxTotal())
			->setShipping($order->GAShippingTotal());

		foreach ($order->Items() as $item) {
			$name = 'unknown';
			$category = 'unknown';
			if ($item->Buyable()) {
				$name = $item->Buyable()->Title;
				$category = $item->Buyable()->Parent()->NestedTitle(5, '/');
			}
			$analytics->addProduct([
				'sku' => $item->ProductID,
				'name' => $name,
				'category' => $category,
				'price' => $item->UnitPrice(),
				'quantity' => $item->Quantity,
			]);
		}

		//$analytics->setProductActionToPurchase();
		//$result = $analytics->setEventCategory('Checkout')
		//	->setEventAction('Purchase')
		//	->sendEvent();
			
		//if ($result instanceof \TheIconic\Tracking\GoogleAnalytics\AnalyticsResponse && $result->getHttpStatusCode() == 200) {
			$order->AnalyticsSubmitted = true;
			$order->write();
			$order->createStatusLog('Order Update', 'OnAuthorized: Success. This order was marked as paid at Google Analytics');
			//$order->createStatusLog('Order Update', 'OnAuthorized: Success. This order was marked as paid at Google Analytics. URL used for the request: ' . $result->getRequestUrl());
		//}
	}

	private function analyticsFactory(Order $order) {
		$analytics = new Analytics(true);

		if($order && $order->Member()){
			$url = AccountPage::get()->first()->Link() . 'order/' . $order->ID;
		} else {
			$url = CheckoutPage::get()->first()->Link() . 'order/' . $order->ID;
		}

		$analytics
			->setProtocolVersion('1')
			->setTrackingId(ShopGoogleAnalytics::get_property_id())
			->setClientId($this->getClientIdFromOrder())
			->setDocumentPath($url);

		return $analytics;
	}

	public function getClientIdFromOrder(){
		$order = $this->owner->Order();
		$clientid = '12345678';
		if($order && $order->GaClientID){
			$clientid = $order->GaClientID;
		}
		return $clientid;
	}

}
