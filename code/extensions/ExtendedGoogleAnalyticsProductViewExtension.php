<?php

/**
 * Created by IntelliJ IDEA.
 * 
 * User: Werner
 * Date: 07.06.2016
 * Time: 08:24
 *
 * @property Product_Controller $owner
 */
class ExtendedGoogleAnalyticsProductViewExtension extends Extension
{
	public function onAfterInit()
	{
		if ($this->owner->GAWebPropertyID()) {
			$this->owner->addGASnippet('ProductDetailView');
		}
	}


}
