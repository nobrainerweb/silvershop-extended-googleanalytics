<?php

/**
 * Created by IntelliJ IDEA.
 *
 * User: Werner
 * Date: 08.06.2016
 * Time: 09:41
 *
 * @property AccountPage_Controller $owner
 */
class ExtendedGoogleAnalyticsAccountPageExtension extends Extension
{


	public function onAfterInit()
	{
		$action = $this->owner->request->param('Action');

		switch ($action) {
			case 'order':
				$this->addAnalyticsIfOrderIsPaid();
				break;

			default:
				break;
		}
	}

	public function addAnalyticsIfOrderIsPaid()
	{
		$order = $this->owner->orderfromid();
		if ($order && !$order->AnalyticsSubmitted && $order->IsPaid()) {
			$this->owner->addGASnippet('CheckoutStepPaymentReceived', $order);
//					$order->AnalyticsSubmitted = true;
//					$order->write();
		}
	}

}
