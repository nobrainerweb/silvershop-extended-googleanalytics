<?php

/**
 * Created by IntelliJ IDEA.
 * 
 * User: Werner
 * Date: 07.06.2016
 * Time: 09:24
 *
 * @property ProductCategory_Controller $owner
 */
class ExtendedGoogleAnalyticsProductListingExtension extends Extension
{
	public function onAfterInit()
	{
		if ($this->owner->GAWebPropertyID()) {
			$this->owner->addGASnippet('ProductList');
		}
	}
}
