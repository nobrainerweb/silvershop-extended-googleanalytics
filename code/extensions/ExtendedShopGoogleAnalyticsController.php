<?php

/**
 * Created by IntelliJ IDEA.
 * 
 * User: Werner
 * Date: 07.06.2016
 * Time: 09:43
 *
 */
class ExtendedShopGoogleAnalyticsController extends ShopGoogleAnalyticsController
{
	public function onAfterInit()
	{
		if (!$this->owner->GAWebPropertyID()) {
			return;
		}

		// record page view if appropriate
		if (!ShopGoogleAnalytics::config()->disable_pageviews) {
			$this->addGASnippet('PageView');
		}

//		//@todo: check if CustomFeaturedProducts is used on this page...
//		if ($this->owner->hasMethod('CustomFeaturedProducts') && $this->owner->CustomFeaturedProducts()) {
//				$this->owner->addGASnippet('FeaturedProductList');
//		}
	}


	/**
	 * @return string
	 */
	public function GoogleAnalyticsJS()
	{
		if (empty($this->snippets)) {
			return '';
		}

		//add page_view...

		$this->snippets[] = "ga('send', 'pageview');";

		return "<script type=\"text/javascript\">\n" . implode("\n", $this->snippets) . "</script>\n";
	}

	public function GASnippet($name)
	{
		$this->addGASnippet($name);
		return '';
	}
}
