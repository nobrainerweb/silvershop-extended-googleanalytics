<?php

/**
 * Created by IntelliJ IDEA.
 * 
 * User: Werner
 * Date: 07.06.2016
 * Time: 09:24
 *
 */
class ExtendedGoogleAnalyticsProductSliderExtension extends Extension
{
	public function onAfterInit()
	{
		if ($this->owner->GAWebPropertyID()) {
			$this->owner->addGASnippet('ProductSlider');
		}
	}
}
