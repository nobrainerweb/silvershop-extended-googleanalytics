<?php

/**
 * Created by IntelliJ IDEA.
 *
 * User: Werner
 * Date: 08.06.2016
 * Time: 09:41
 *
 * @property CheckoutPage_Controller $owner
 */
class ExtendedGoogleAnalyticsCheckoutExtension extends Extension
{
	private static $checkout_steps = [
		'1' => 'Checkout Cart',
		'2' => 'Payment Received',
	];


	public function onAfterInit()
	{
		$action = $this->owner->request->param('Action');

		switch ($action) {
			case 'OrderForm':
				break;

			default:
				//checkout
				$this->owner->addGASnippet('CheckoutStepCheckout');
		}
	}
}
